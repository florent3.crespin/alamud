# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event1

class ToquerEvent(Event1):
    NAME = "toquer"

    def perform(self):
        self.inform("toquer")
